<?php

namespace App\Observers;

use App\Customer;

use App\User;



class CustomerObserver
{
    public function creating(Customer $customer)

    {
        $customer->user_id = auth()->id();}

    /**
     * Handle the customer "updated" event.
     *
     * @param  \App\Customer  $customer
     * @return void
     */
    public function updated(Customer $customer)
    {
        if($customer->user_id == auth()->id())
            return true;
        else
            return false;

    }

    /**
     * Handle the customer "deleted" event.
     *
     * @param  \App\Customer  $customer
     * @return void
     */
    public function deleted(User $user,Customer $customer)
    {
        if($customer->user_id == auth()->user()->id)
            return true;
        else
            return false;

    }

    /**
     * Handle the customer "restored" event.
     *
     * @param  \App\Customer  $customer
     * @return void
     */
    public function restored(Customer $customer)
    {
        //
    }

    /**
     * Handle the customer "force deleted" event.
     *
     * @param  \App\Customer  $customer
     * @return void
     */
    public function forceDeleted(Customer $customer)
    {
        //
    }

}
